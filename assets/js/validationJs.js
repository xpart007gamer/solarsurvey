//Second Section
//Zip
const surveyForm = document.getElementById('surveyForm');
const zip = document.getElementById('zip');
const city = document.getElementById('city');
const state = document.getElementById('state');





//set Zip Error

const setZipError = (element, message) => {
    
    const input_control_section_2 = element.parentElement;
    const errorDisplay = input_control_section_2.querySelector('.zipError');

    errorDisplay.innerText = message;
    input_control_section_2.classList.add('zipError');
    input_control_section_2.classList.remove('success')
}

const setZipSuccess = element => {
    const input_control_section_2 = element.parentElement;
    const errorDisplay = input_control_section_2.querySelector('.zipError');

    errorDisplay.innerText = '';
    input_control_section_2.classList.add('success');
    input_control_section_2.classList.remove('zipError');
};



// //set City Error
const setCityError = (element, message) => {
   
  
    const input_control_section_2 = element.parentElement;
    const errorDisplay = input_control_section_2.querySelector('.cityError');

    errorDisplay.innerText = message;
    // input_control_section_2.classList.add('cityError');
    // input_control_section_2.classList.remove('success')
}

const setCitySuccess = element => {
    const input_control_section_2 = element.parentElement;
    const errorDisplay = input_control_section_2.querySelector('.cityError');

    errorDisplay.innerText = '';
    input_control_section_2.classList.add('success');
    input_control_section_2.classList.remove('cityError');
};


// //set State Error
const setStateError = (element, message) => {
   
  
    const input_control_section_2 = element.parentElement;
    const errorDisplay = input_control_section_2.querySelector('.stateError');

    errorDisplay.innerText = message;
    // input_control_section_2.classList.add('cityError');
    // input_control_section_2.classList.remove('success')
}

function surveySectionTwoNext()
{

    debugger;
   
    let zipvalue=zip.value;
    let sectionNineZipValue=document.getElementById('SectionNineInputZip');
    debugger;
        if(zipvalue!='')
        {

            // debugger;
            // var servyFormHeight=document.getElementsByClassName("survey-form").style.backgroundColor='red';
            // servyFormHeight.style.backgroundColor='red';
            // zip.style.borderColor="red";


            if (zipvalue.length>=5)
            {
                $('.survey-section-2').hide();
                $('.survey-section-3').show();
    
            }
            else
            {
                setZipError(zip, 'Please enter minimum 5 Digit. (i.e. 90210)');
            let optionalFiled=document.getElementById('optionalField');
            if (optionalFiled.style.display === "block") {
                optionalFiled.style.display = "none";
            } 
            
            else {
                optionalFiled.style.display = "block";
            }

            city.style.borderColor="red";
              setCityError(city, 'Please complete this field.');

              state.style.borderColor="red";
              setStateError(state, 'Please complete this field.');
            }

             

           
    
            // validateZipInputs();
            // validateCityInputs();
            // validateStateInputs();
        }
        else
        { 
            // setZipSuccess(zip);
            // sectionNineZipValue.innerHTML="Value = " + "'" + zipvalue + "'";

           


            setZipError(zip, 'Please enter a valid ZIP Code. (i.e. 90210)');
            let optionalFiled=document.getElementById('optionalField');
            if (optionalFiled.style.display === "block") {
                optionalFiled.style.display = "none";
            } 
            
            else {
                optionalFiled.style.display = "block";
            }

            city.style.borderColor="red";
              setCityError(city, 'Please complete this field.');

              state.style.borderColor="red";
              setStateError(state, 'Please complete this field.');
        }

}


// zipform.addEventListener('button', e => {

  
    
//     e.preventDefault();
//     let zipvalue=zip.value;
//     if(zipvalue=='')
//     {
       
//         let optionalFiled=document.getElementById('optionalField');
//         if (optionalFiled.style.display === "block") {
//             optionalFiled.style.display = "none";
//         } else {
//             optionalFiled.style.display = "block";
//         }
       

//         validateZipInputs();
//         validateCityInputs();
//         validateStateInputs();
//     }
//     else
//     { setZipSuccess(zip);
//         $('.survey-section-2').hide();
//         $('.survey-section-3').show();
//     }
    
// });

//input_control_section_2





//ZipValidate

// const validateZipInputs = () => {
   
//     zip.style.borderColor="red";
//    setZipError(zip, 'Please enter a valid ZIP Code. (i.e. 90210)');
      
       
//     }
//     //ZipValidate

// const validateCityInputs = () => {
   
//     city.style.borderColor="red";
//     setCityError(city, 'Please complete this field.');
       
        
//      }

//      //StateValidate
//      const validateStateInputs =()=>
//      {
//         state.style.borderColor="red";
//         setCityError(state, 'Please complete this field.');
//      }
 

   








//Mail

const form = document.getElementById('emailForm');
const email = document.getElementById('Email');


function surveySectionEightNext()
{
   
    

    let emailvalue=email.value;
    if(emailvalue=='')
    {
        email.style.borderColor="red";
        validateInputs();
    }
    else
    {
        let browserName=navigator.appCodeName;
        let browserProduct=navigator.product;
        let browserVersion=navigator.appVersion;
        let userAgent=document.getElementById("userAgent");
  
        userAgent.value=browserName+" "+browserProduct+" "+browserVersion
        $('.survey-section-8').hide();
        $('.survey-section-9').show();
    }
    
}

const setError = (element, message) => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = message;
    inputControl.classList.add('error');
    inputControl.classList.remove('success')
}

const setSuccess = element => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = '';
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};

const isValidEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const validateInputs = () => {
    
    const emailValue = email.value.trim();
  

    
    if(emailValue === '') {
        setError(email, 'Email is required');
    } else if (!isValidEmail(emailValue)) {
        setError(email, 'Provide a valid email address');
    } else {
        setSuccess(email);
    }

   

};

//streetAddressValue


let streetAddress=document.getElementById('inputStreet');
let SectionNineInputZip=document.getElementById('SectionNineInputZip');
let inpuTCity=document.getElementById('inpuTCity');
let inputState=document.getElementById('inputState');


function surveyNineNextButton()
{
    
    let streetAddressValue=streetAddress.value;
    let sectionNineStreet=document.getElementById('inputState');
  
    if(streetAddressValue=='')
    {
        let optionalFiled=document.getElementById('sectionNineOptionalFiled');
        let btnChange=document.getElementById('ChangeBtn');

 
        validateMessageStreetAddress();

        streetAddress.style.borderColor="red";
        SectionNineInputZip.style.borderColor="red";
        inpuTCity.style.borderColor="red";
        inputState.style.borderColor="red";
        btnChange.style.display="none";
        if (optionalFiled.style.display === "block") {
            optionalFiled.style.display = "none";
        } else {
            optionalFiled.style.display = "block";
        }
    }
    else
    {
      
    
      
      
        debugger;
        var Fulladdress=streetAddress.value;
        const splitArr = Fulladdress.split(",");
        const address =splitArr[0];
        const cityValuE =splitArr[1];
        const stateValue =splitArr[2];


        $(streetAddress).val(address);
        $("#state option:nth-child(1)").val(stateValue).attr("selected", "selected");
         
        city.setAttribute('value', cityValuE);

        

        $('.survey-section-9').hide();
        $('.survey-section-10').show();
    }

 



function validateMessageStreetAddress ()
{
    
    let streetAddressElement=streetAddress;
    const inputControl = streetAddressElement.parentElement;
    const errorDisplay = inputControl.querySelector('.streetAddressErrorMessage');
    errorDisplay.innerText = 'Please complete this field.';


   // setStreetAddressError(streetAddress, 'Please complete this field.');
} 

}



//Section 10 Validation


let firstName=document.getElementById('inputFirstName');
let lastName=document.getElementById('inputLastName');


function surveyTenNext()
{
    debugger;
    let firstNameValue=firstName.value;
    let lastNameValue=lastName.value;
    
    
    if(firstNameValue!='' && lastNameValue!='' )
    {
        $('.survey-section-10').hide();
        $('.survey-section-11').show();


        
    }
    else
    {
       
        firstName.style.borderColor="red"
        lastName.style.borderColor="red"
        validateMessageNameSection();
       
    }

}


const setErrorFirstName=(element,message)=> {
    
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.streetAddressErrorMessage');
    errorDisplay.innerText = message;
}

const setErrorLastName=(element,message)=> {
    
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.streetAddressErrorMessage');
    errorDisplay.innerText = message;
}

// const setError = (element, message) => {
//     const inputControl = element.parentElement;
//     const errorDisplay = inputControl.querySelector('.error');

//     errorDisplay.innerText = message;
//     inputControl.classList.add('error');
//     inputControl.classList.remove('success')
// }






function  validateMessageNameSection ()
{
   
    setErrorFirstName(firstName,'First name is required.')
    setErrorLastName(lastName,'Last name is required.')
    // let streetAddressElement=firstName;
    // const inputControl = streetAddressElement.parentElement;
    // const errorDisplay = inputControl.querySelector('.streetAddressErrorMessage');
    // errorDisplay.innerText = 'Please complete this field.';


   // setStreetAddressError(streetAddress, 'Please complete this field.');
} 



//Last Section Phone Number

// let phoneNumber=document.getElementById('inputPhoneNumber');
// function surveyResult()
// {

//     debugger;
 
   
// let phoneNumberValue=phoneNumber.value;
// if(phoneNumberValue=='')
// {
//     inputValidateMessagePhoneNumber();
// }
// else
// {
   
//     window.location.href = "../offer.html";
// }
// }

// const setErrorMessagePhoneNumber=(element,message)=> {
   
//     const inputControl = element.parentElement;
//     const errorDisplay = inputControl.querySelector('.streetAddressErrorMessage');
//     errorDisplay.innerText = message;
// }

// function inputValidateMessagePhoneNumber()
// {
   
//     setErrorMessagePhoneNumber(phoneNumber,'Please complete this field.');
// }





//ChangeBtn
//surveySectionNine

 let ChangeBtn=document.getElementById('ChangeBtn');

 ChangeBtn.onclick=function()
{
   debugger;
   
      
       let zipCodeReview=document.getElementById('SectionNineInputZip');
       let streetReview=document.getElementById('inputStreet');
       let cityReview=document.getElementById('inpuTCity');
       let stateReview=document.getElementById('inputState');

       zipCodeReview.setAttribute('value', zip.value);
       cityReview.setAttribute('value', city.value);
       stateReview.setAttribute('value', state.value);
     


      $('#sectionNineOptionalFiled').show();
      ChangeBtn.style.display = 'none';
    
 }